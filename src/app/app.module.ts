import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

//Peticiones https
import { HttpClientModule } from "@angular/common/http";

//Rutas
import { AppRoutingModule } from './app-routing.module';

//Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { BannerComponent } from './components/shared/banner/banner.component';
import { CategoriaComponent } from './components/shared/categoria/categoria.component';
import { MarcasComponent } from './components/shared/marcas/marcas.component';
import { SubmenuComponent } from './components/shared/submenu/submenu.component';
import { MenunegroComponent } from './components/shared/menunegro/menunegro.component';
import { SwiperModule } from 'swiper/angular';
import { ProductosComponent } from './components/shared/productos/productos.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BannerComponent,
    CategoriaComponent,
    MarcasComponent,
    SubmenuComponent,
    MenunegroComponent,
    ProductosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SwiperModule,
    HttpClientModule
  ],
  providers: [
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
