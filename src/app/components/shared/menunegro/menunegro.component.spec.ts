import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenunegroComponent } from './menunegro.component';

describe('MenunegroComponent', () => {
  let component: MenunegroComponent;
  let fixture: ComponentFixture<MenunegroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenunegroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenunegroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
