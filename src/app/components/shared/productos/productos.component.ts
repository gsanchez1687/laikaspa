import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
  
})
export class ProductosComponent implements OnInit {

  public productos:any[] = [];
  private url_api = 'http://127.0.0.1:8000/api/productos';

  constructor( private http: HttpClient ) {
    this.http.get(this.url_api)
    .subscribe( (data:any) => {
      this.productos = data;
        console.log(data);
      }
    )
   }

  ngOnInit(): void {
   
  }

}
